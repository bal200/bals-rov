# Bals-ROV - The Long Distance Underwater ROV 
###An ROV that takes itself out to sea, no need to own a boat!

This project is the Raspberry Pi software to accompany an underwater ROV submarine project in development.  The ROV uses a Raspberry Pi computer fitted inside a slim hull made from standard plumbing components, with sensors and maneuvered by 3 external motors.  The ROV is tethered to a floating buoy above on the surface.  This buoy has an antennae and creates a long distance WiFi connection to your laptop ashore, where you can control it through a web browser.

[Main project page](http://balsrov.wordpress.com/)

![lake screenshot](https://bytebucket.org/bal200/bals-rov/raw/ee5b64684412b37889be3c99e80772de42ddc247/photos/lakescreenshot.jpg)

This configuration solves the problem of radio waves not travelling through water, but also not restricting the travel distance with cables.

You connect to the ROV with a web browser. This shows the cockpit view of your ROV, showing the live camera feed, controls to move your ROV and telemetry data.  A PC Gamepad can be used to move the ROV too.

The TCPIP connection will be made in two steps.  An Ethernet cable connection to the other Pi fitted into a Buoy floating on the surface.  This will run router software and make the second connection via WiFi antennae to the user on-shore.

The ROV is a slim shape designed to travel on the surface much better then conventional box shaped ROV's.  Coupled with the long distance Wifi link, the ROV can explore a remote ocean site of interest, while the user stays ashore.  No need to own a boat. 

The project source code runs on a Node.js server installed on the ROV's Raspberry Pi.  The buoy will have a second Raspberry Pi configured as a simple router (plus possibly some other node.js code for a future GPS location receiver)

## Features ##
* MJPEG live camera feed from a RaspiCam connected to your Pi
* Control the ROV with left/right, forward/reverse, dive and rise controls
* Horizon level line shows over the video to help with underwater orientation (HUD)

## Future ##
* Compass direction added to the HUD
* use a Game-pad to control the ROV
* Pressure sensor input to show water Depth
* Head Light control
* GPS sensor and on-screen map

### Hardware required for the Pi ###
The hardware for the actual ROV is being developed separately with no guide as yet.  This software could potentially control a ROV project with similar components.  Here is a list of the minimum hardware needed connected to the Pi.  The layout configuration of the 3 motor thrusters would need to match the below too.

 * [RaspiCam](https://www.raspberrypi.org/products/camera-module/) camera module
 * [XloBorg](https://www.piborg.org/xloborg) (compass & accelerometer addon for Pi)
 * An ethernet cable, or WiFi dongle for the Pi
 * R/C speed controllers (x3), with matching brushless motors
    - Speed controllers are connected directly to the Pi's GPIO pins
    - 2 motors are mounted pointing forward, one on the left and one on the right.
    - Both are run together for forward/reverse movement.  Or they are run in opposite directions for turning left and right (yaw control).
    - 1 motor is mounted at the back pointing down.  This tilts the sub to point up or down, to dive or rise (pitch movement).

All other components to build a submersible remotely controlled vehicle are outside of this scope.

### Dependencies ###
 - Node.js
 - Video4Linux (uv4l)
 - pi-servo-blaster
 - socket.io


## Install Instructions ##
The project has been built on a Raspberry Pi B+ running the default Raspian OS.  Currently the project will run as a web service on one Raspberry Pi; the surface buoy with a second Pi is still in development.  These instructions will setup the sub Pi, and network cable tether.  You just need to plug the other end of the ethernet cable into a laptop.

The Pi will sort out the IP connection.  You can then point a web browser at a fixed IP address to access the sub.

#### Install and setup the Sub Pi
>**Note:** I use my Raspberry Pi "Headless", so without a screen attached.  I connect to it over SSH using [Putty](http://www.chiark.greenend.org.uk/~sgtatham/putty/) on Windows.  Start with A Running Pi with a fresh install of the Raspian OS.

Logon to your Pi, and run the config utility to enable the I2C bus and enable SSH:
```
sudo raspi-config
enable I2C
enable I2C load by default
enable SSH
```

####install Node.JS

The apt-get version is out of date, so I had to build the latest version myself (v0.10.2). Follow the guide here to install NodeJS: http://elinux.org/Node.js_on_RPi

Also update NPM, which comes with NodeJS, but needed updating.  v3.8.0 is working for me:
```
sudo npm install npm -g
```

####Install the [xloborg](https://www.piborg.org/xloborg) driver
```
mkdir ~/xloborg
cd ~/xloborg
wget http://www.piborg.org/downloads/xloborg/examples.zip
unzip examples.zip
chmod +x install.sh
./install.sh
```
####Install [ServoBlaster](https://github.com/richardghirst/PiBits/tree/master/ServoBlaster), which talks to the speed controllers
```
cd ~
git clone https://github.com/richardghirst/PiBits.git
cd PiBits/ServoBlaster/user/
make ./servod
sudo make install
```

####Install UV4L to serve the camera feed

Follow the UV4L install guide here: 
http://www.linux-projects.org/modules/sections/index.php?op=viewarticle&artid=14
Include the Extras, as you want it to start at boot. Also install the server add on, and the mjpeg streamer: 
```
sudo apt-get install uv4l-raspicam-extras
sudo apt-get install uv4l-server
sudo apt-get install uv4l-mjpegstream
```
Note, you may need to enable the camera back in the raspi-config:
```
sudo raspi-config
enable camera
```

Edit the UV4L config file:
```
sudo nano /etc/uv4l/uv4l-raspicam.conf
```
Uncoment and change these values:
width=640
height=480
framerate = 20
quality = 20


####DHCP Server
To make connecting to the sub from your laptop easier, we create a DHCP server on the sub.  So when you connect the other end of the subs ethernet cable tether to your laptop, the IP configuration is automatic.  This link is separate to any internet connection your laptop may have, meaning you can go to websites and access the sub at the same time.

```
sudo apt-get install isc-dhcp-server
```
Edit /etc/default/isc-dhcp-server , and add to the bottom:
```
INTERFACES="eth0"
```

Edit /etc/network/interfaces .  Comment-out any other eth0 lines, then add:
```
# Create our subs static IP
auto eth0
iface eth0 inet static
    address 192.168.36.1
    netmast 255.255.255.0
    broadcast 192.168.36.255
```
Edit /etc/dhcp/dhcpd.conf and add the below:
```
subnet 192.168.36.0 netmask 255.255.255.0 {
  range 192.168.36.10 192.168.36.50;
  option subnet-mask 255.255.255.0;
  option broadcast-address 192.168.36.255;
}
authoritive;
```
Start the services for the DHCP server
```
sudo service isc-dhcp-server stop
sudo service isc-dhcp-server start
sudo ifdown eth0
sudo ifup eth0
```


####Install the bals-rov server:
```
cd ~
git clone https://bal200@bitbucket.org/bal200/bals-rov.git
cd bals-rov
make
sudo make install
```
Reboot your Pi.

## Running

Currently the project requires an ethernet cable the controling laptop and the ROV.  In the future the wifi
Buoy will be in the middle.  But for now, connect an ethernet cable between your rov's Pi and the laptop you'll control it with.
If you setup the DHCP components above, you don't need to do any IP configuring.  You can just type the ROV's
address into a modern browser on your laptop:

```
http://192.168.36.1
```
The '36' number was picked so it doesn't clash with your home subnet.  All being well, you'll see a webpage served up
from the Pi, which will be the Cockpit view of your ROV.

To restart the bals-rov webpage and/or restart the camera driver (replace restart with stop or start if required):
```
sudo service bals-rov restart
sudo service uv4l-raspicam restart
```
The arrow buttons will control the speed controllers, which will control your motors.
You're speed controllers may need calibrating before use, follow the manufacturers instructions.

The speed controllers wiring is identical to a servo, hence why the servo-blaster driver is used.  See part two of [this page](http://cihatkeser.com/servo-control-with-raspberry-pi-in-5-minutes-or-less/) for a demo of wiring servos/speed controllers to a Pi.  I have used servo channel numbers 0,1 & 2 for the three motors in this project.  For reference, here is the pin-outs on the Pi you'll need to connect the speed controllers signal lines to:
```
     Servo number    GPIO number   Pin in P1 header
          0               4             P1-7
          1              17             P1-11
          2              18             P1-12
          3             21/27           P1-13
          4              22             P1-15
          5              23             P1-16
          6              24             P1-18
          7              25             P1-22
``` 


### Horizon Line
The Sensor Data button will toggle
on/off the sending of telemetry data from the sub Pi.  You need an XloBorg plugged in.  This currently will display an overlay
on the camera view, which has a Horizon line, also called a "Heads up display" or HUD.  This green line
will move depending on the orientation of the Sub, which greatly helps with underwater orientation.  Ensure
the xloborg sensor board is fixed and aligned correctly in the subs hull, otherwise your line will be off!

The Video Settings button takes you to the UV4L video streaming software's settings pages.  To change the
resolution and frame rate, Goto the Control panel button in the settings.

### Game pad
A USB Control pad, used for computer games can be used to control the sub. This will give analogue control of
the motors and will mean much easier maneuverability.

This is in experimental stages at the minute.  Different control pads have different button mappings, so I currently only works with the particular control pad I have.  A method of mapping the buttons to each movement is planned for the future.