var socket = io();

/* Global variables */
var PI=3.141592654;
var sensorTransmit = false;
var overlay, canvas; /* the canvas HUD overlay context */
var x1=0,y1=0, x2=0,y2=0; /* hold the coords of the HUD line */
var clearTO=null; /* a timout variable to refresh screen after timeout */
var keydown=false;

var leftright=0;  /* store the current movement values */
var updown=0;
var forwardback=0;

/* Initialise overlay Canvas, used to draw HUD over video */
function init(){
  canvas = document.getElementById('overlay');
  overlay = canvas.getContext('2d');

  if (!document.addEventListener && document.attachEvent) {
	  document.attachEvent('onkeydown', handleKeyDown); //IE
	  document.attachEvent('onkeyup', handleKeyUp);
  } else {
	  window.addEventListener('keydown', handleKeyDown, false);
	  window.addEventListener('keyup', handleKeyUp, false);
  }
  /* Initialie the Gamepad API for a control pad */
  var hasGP = false;
  var gamepadInterval = null;
  var lastGamepad = new Array();
 
  function canGame() {
    return "getGamepads" in navigator;
  }
 
  if(canGame()) {
    function gamepadLoop() {
      gamepad = navigator.getGamepads()[0];
      if(gamepad){
        var change=false;
        /* Left Right stick*/
        if (lastGamepad[0] != gamepad.axes[0]) {
          leftright = gamepad.axes[0] * 50.0; /* convert to -50 to +50 */
          change=true;
        /* Up Down stick */
        }else if (lastGamepad[1] != gamepad.axes[1]) {
          updown = gamepad.axes[1] * 50.0; change=true;
        /* forward back stick */
        }else if (lastGamepad[5] != gamepad.axes[5]) {
          forwardback = gamepad.axes[5] * 50.0 *-1; change=true;
        }
        /* Forward button */
        if (lastGamepad[101] != gamepad.buttons[1].pressed) {
          change=true;
          if (gamepad.buttons[1].pressed)
            forwardback = +40; else forwardback = 0;
        }
        /* Reverse button */
        if (lastGamepad[102] != gamepad.buttons[2].pressed) {
          change=true;
          if (gamepad.buttons[2].pressed)
            forwardback = -40; else forwardback = 0;
        }
        /* D-Pad Left & Right buttons */
        if (gamepad.buttons[14]) {
          if (lastGamepad[114] != gamepad.buttons[14].pressed) {
            change=true;
            if (gamepad.buttons[114].pressed)
              leftright = -40; else leftright = 0;
          }else if (lastGamepad[115] != gamepad.buttons[15].pressed) {
            change=true;
            if (gamepad.buttons[115].pressed)
              leftright = +40; else leftright = 0;
          }
        }
        /* D-Pad Up Down buttons */
        if (gamepad.buttons[12]) {
          if (lastGamepad[112] != gamepad.buttons[12].pressed) {
            change=true;
            if (gamepad.buttons[112].pressed)
              updown = +40; else updown = 0;
          }else if (lastGamepad[113] != gamepad.buttons[13].pressed) {
            change=true;
            if (gamepad.buttons[113].pressed)
              updown = -40; else updown = 0;
          }
        }
        lastGamepad[0] = gamepad.axes[0];
        lastGamepad[1] = gamepad.axes[1];
        lastGamepad[5] = gamepad.axes[5];
        lastGamepad[101] = gamepad.buttons[1].pressed;
        lastGamepad[102] = gamepad.buttons[2].pressed;
        if (gamepad.buttons[14]) {
          lastGamepad[114] = gamepad.buttons[14].pressed;
          lastGamepad[115] = gamepad.buttons[15].pressed;
          lastGamepad[112] = gamepad.buttons[12].pressed;
          lastGamepad[113] = gamepad.buttons[13].pressed;
        }
        if (change) sendMoves();
      }
    }

    window.addEventListener("gamepadconnected", function(e) {
      hasGP = true;
      console.log("Gamepad connected", e.gamepad);
      gamepadInterval=window.setInterval(gamepadLoop, 100);
    });
    window.addEventListener("gamepaddisconnected", function(e) {
      console.log("Gamepad disconnected", e.gamepad);
      window.clearInterval(gamepadInterval);
      gamepadInterval=null;
    });
    //setup an interval for Chrome
    //var checkGP = window.setInterval(function() {
    //    if(navigator.getGamepads()[0]) {
    //        if(!hasGP) $(window).trigger("gamepadconnected");
    //        window.clearInterval(checkGP);
    //    }
    //}, 500);
  }
 
};

/*
client sends all channels on regular Timeout, and when changes happen:
data range is -50 to +50:
eg.: leftright: -35
updown: +5
forwardback: +0

Server converts this into correct servo channels (mixing)
Server timeout if no signal received for 500ms, resets all to 0%
*/
function sendMoves() {
  socket.emit('move', {
    lr : leftright,
    ud : updown,
    fb : forwardback
  } );
}
function left() {
  leftright = -45;
  sendMoves();
};
function right() {
  leftright = +45;
  sendMoves();
};
function horizCentre() {
  leftright = 0;
  sendMoves();
};
function up() {
  updown = +45;
  sendMoves();
};
function down() {
  updown = -45;
  sendMoves();
};
function vertCentre() {
  updown = 0;
  sendMoves();
};
function forward() {
  forwardback = +50;
  sendMoves();
};
function backward() {
  forwardback = -50;
  sendMoves();
};
function powerCentre() {
  forwardback = 0;
  sendMoves();
};
/* Tell the Server to stop or start sending the continuous
 * sensor data  */
function sensorDataToggle() {
  if (sensorTransmit == false) {
    sensorTransmit=true;
    setSensorTransmitBadge( sensorTransmit );
    socket.emit('sensortransmit', { turn : 'on'} );
  } else {
    sensorTransmit=false;
    setSensorTransmitBadge( sensorTransmit );
    socket.emit('sensortransmit', { turn : 'off'} );
  }
  return false;
};
function setSensorTransmitBadge(val) {
  if (val) {
    document.getElementById("buttonSensorData").src="js/button_with_badge_yellow.png";
  }else{
    document.getElementById("buttonSensorData").src="js/button_with_badge.png";
  }
}

/* Receive a data packet, and draw the OSD */
socket.on('data', function(data){
  var vidWidth=640;
  var vidHeight=480;
  var lensAngle=40 /2; /* the vertical field of view of the Camera, so the on screen display is positioned correct.
                        * RaspiCam is 40.  Keep the /2 , as we need it halved */

  function drawLine(lineLength, angle, colour) {
    /* calc the line matrix */
    linex=Math.cos(twistAngle) * lineLength; 
    liney=Math.sin(twistAngle) * lineLength;
    /* calc the height of line on screen */
    h = midHeight * ((horizAngle + angle) / lensAngle);  
    /* calc x,y's for line */
    x1 = midWidth - linex;  y1 = midHeight -h -liney;
    x2 = midWidth + linex;  y2 = midHeight -h +liney;
    /* draw line */
    overlay.strokeStyle = colour;
    overlay.beginPath();
    overlay.moveTo(x1, y1);
    overlay.lineTo(x2, y2);
    overlay.stroke();    
  }
  function drawCircle(radius, angle, colour) {
    /* calc the height of line on screen */
    h = midHeight * ((horizAngle + angle) / lensAngle);  
    /* draw line */
    overlay.strokeStyle = colour;
    overlay.beginPath();
    overlay.arc(midWidth, midHeight-h, radius, 0,2*Math.PI,false);
    overlay.stroke();    
  }
  
  if (data) {
    //setSensorTransmitBadge(true);
    if (clearTO) clearTimeout(clearTO);
    clearTO = setTimeout(clearOverlay, 2000);
    /* Delete the previous line */
    overlay.clearRect(0, 0, canvas.width, canvas.height);

    /* Work out twist angle (ROLL) */
    twistAngle = Math.atan2(data.acc.y, data.acc.z);
    //twistAngle *= (180/PI);
    
    /* Work out line Height (PITCH) */
    horizAngle = Math.atan2(data.acc.x, data.acc.z);
    horizAngle *= (180/PI);
    
    var midWidth = vidWidth /2;
    var midHeight = vidHeight /2;

    /* Horizon green line */
    drawLine(vidWidth*0.3, 0, "rgb(0,200,0)");
    /* DOWNWARD red lines */
    drawLine((vidWidth*0.1), -22.5, "rgb(200,0,0)");
    drawLine((vidWidth*0.2)  , -45,   "rgb(200,0,0)");
    drawLine((vidWidth*0.1), -67.5, "rgb(200,0,0)");
    drawCircle((vidWidth*0.03), -90,   "rgb(200,0,0)");
    
    /* UPWARD blue lines */
    drawLine((vidWidth*0.1), +22.5, "rgb(0,0,200)");
    drawLine((vidWidth*0.2)  , +45,   "rgb(0,0,200)");
    drawLine((vidWidth*0.1), +67.5, "rgb(0,0,200)");
    drawCircle((vidWidth*0.03), +90,   "rgb(0,0,200)");
    
    var str = "acc x:"+data.acc.x
               +" y:"+data.acc.y
               +" z:"+data.acc.z
               +"</br> horizAngle:"+horizAngle
               +"</br> twistAngle:"+twistAngle ;
    document.getElementById("txt").innerHTML = str;
    
  }
  
  function clearOverlay() {
    overlay.clearRect(0, 0, canvas.width, canvas.height);
  }
  
});


function handleKeyDown(event) {
    //if (keydown == false) {
      if (event.keyCode == 38)  up();
      else if (event.keyCode == 40)  down();
      else if (event.keyCode == 37)  left();
      else if (event.keyCode == 39)  right();
      else if (event.keyCode == 173 || event.keyCode==189) {} // minus
      else if (event.keyCode == 61 || event.keyCode==187) {} // plus
      else if (event.keyCode == 90) forward(); // Z
      else if (event.keyCode == 88) backward(); // X
      else if (event.keyCode == 49) {} // "1" 
      else if (event.keyCode == 50) {} // "2" 
      //else alert("keycode: "+event.keyCode);
     keydown = true;
    //}
    return false;
}
function handleKeyUp(event) {
  //alert("keycode: "+event.keyCode);
  keydown = false;
  if (event.keyCode == 38)  vertCentre(); // up
  else if (event.keyCode == 40) vertCentre(); // down
  else if (event.keyCode == 37) horizCentre(); // left
  else if (event.keyCode == 39) horizCentre(); // right
  else if (event.keyCode == 90) powerCentre(); // Z, 
  else if (event.keyCode == 88) powerCentre(); // X, 
  return false;
}




