
var PORT = 80; /* TCP port number for this webpage to be served on */
var VIDEO_PORT = 8080; /* The UV4L video stream is hosted seperately, set this to its port number */
var TRANSMIT_FREQ = 200; /* how often sensor data is sent to the client, in milliseconds (1000ms=1sec) */

var ACC_X =0;  /** If the accelerometer is mounted at a different angle in the hull   */
var ACC_Y =1;  /** you can swap the axis's here. The numbers are which order the figures */
var ACC_Z =2;  /** are read from the xloBorg device */
var ACC_X_REV = -1;  /* to reverse an axis, set to -1.  To keep axis unchanged set to +1 */
var ACC_Y_REV = -1;
var ACC_Z_REV = +1;

var PI=3.141592654;

var express = require("express");
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var piServoblaster = require('pi-servo-blaster.js');
var xloborg = require('xloborg');

var sensorTransmitInt;
var sensorReadings=null;
var servoTO=null;  // Timout to put srvo back to neutral if no update received in time
var channel = [0,0,0];

/* serve up the home page and its dependants */
app.use("/css", express.static(__dirname + "/css"));
app.use("/js", express.static(__dirname + "/js"));
app.get('/', function(req, res){
  console.log("/ requested");
  res.sendFile(__dirname + '/index.htm');
});

/* for the Video stream, redirect to the UV4L server port */
app.get("/video.mjpeg", function(req, res) {
  console.log("video.mjpeg requested");
  var str = req.headers.host.split(":");
  res.redirect(301,
    'http://' + str[0] +':'+VIDEO_PORT +'/stream/video.mjpeg'
  );
  //return;
});
app.get("/video.h264", function(req, res) {
  console.log("video.h264 requested");
  var str = req.headers.host.split(":");
  res.redirect(301,
    'http://' + str[0] +':'+VIDEO_PORT +'/stream/video.h264'
  );
});

/* for the Video stream, redirect to the UV4L server port */
app.get("/video_settings.htm", function(req, res) {
  console.log("video_settings requested");
  var str = req.headers.host.split(":");
  res.redirect(301, 'http://' + str[0] +':'+VIDEO_PORT +'/' );
  //return;
});

// prepare the XLoBorg compass and accelerometer
xloborg.init(function(err){
    if (err){
      console.error("XLoBorg error: "+err);
    } else {
      console.log('XLoBorg initialised');
    }
});

/***** Mixing Functions *******
 ***** Leave just one servoMixing functon UnCommented.  These functions will convert the input
 ***** positions from the keyboard or Gamepad values into the actual servo channels.
// Channels: 0=left thruster  1=right thruster 2=updown
/* input data range -50 to +50.
   output Channels range 100 to 200 */

/* No mixing - 1-1 mapping */
//function servoMixing(d) { 
//  channel[0] = d.fb ;
//  channel[1] = d.lr ;
//  channel[2] = d.ud ;
//}
/* Mix channels 0&1, the left and right forward thrusters */
function servoMixing(d) {
  /* mixing */
  channel[0] = (d.fb*0.80) + ((d.lr)*0.60);
  channel[1] = (d.fb*0.80) + ((-d.lr)*0.60);

  /* no mixing on up/down channel */
  channel[2] = (d.ud);
  
  /* reverse the channels.  uncomment each line if your motors spin backwards */
  channel[0]= -channel[0];
  channel[1]= -channel[1];
  channel[2]= -channel[2];
}

function convertServoRanges() {
  /* covert to the output ranges, eg. between 100 and 200 */
  channel[0]+=150;  channel[1]+=150; channel[2]+=150;
  /* clip the outputs to the maximums */
  if (channel[0]>200) channel[0]=200; if (channel[0]<100) channel[0]=100; 
  if (channel[1]>200) channel[1]=200; if (channel[1]<100) channel[1]=100; 
  if (channel[2]>200) channel[2]=200; if (channel[2]<100) channel[2]=100; 
}

function setServos() {
  piServoblaster.setServoPwm(0, channel[0]+"");
  piServoblaster.setServoPwm(1, channel[1]+"");
  piServoblaster.setServoPwm(2, channel[2]+"");
  /* Lights on 3? */
/*
Servo number    GPIO number   Pin in P1 header
          0               4             P1-7
          1              17             P1-11
          2              18             P1-12
          3             21/27           P1-13
          4              22             P1-15
          5              23             P1-16
          6              24             P1-18
          7              25             P1-22
*/
}
/* setup socket connection */
io.on('connection', function(socket){
  console.log('a user connected');
  
  /* when we receive a move command from web page */
  socket.on('move', function(data){
    console.log("move received");
    servoMixing(data);
    /* Channel 0: updown
     * Channel 1: leftright
     * Channel 2: forwardback
     **/
    convertServoRanges();
    setServos();
    
  });
  /* on/off message from client to sub to toggle */
  socket.on('sensortransmit', function(data){
    if (data.turn) {
      if(data.turn == 'on') {
        console.log("turning sensor data transmitting ON");
        sensorTransmitInt = setInterval(readSensorsAndTransmit, TRANSMIT_FREQ);
      }else if (data.turn == 'off') {
        console.log("turning sensor data transmitting OFF");
        clearInterval(sensorTransmitInt);
      }
    } else if (data.transmitfrequency) {
      // change the transmit frequency
    }
    
  });
  
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

http.listen(PORT, function(){
  console.log('listening on port '+PORT);
});

// get a reading from the accelerometer and compass and emit the data to te client
function readSensorsAndTransmit() {
  sensorReadings = {};
  // read the accelerometer
  xloborg.readAccelerometer(function(err, result){
    if (err){
        console.error(err);
    } else {
      sensorReadings.acc = {
        x: (result[ACC_X] * ACC_X_REV),
        y: (result[ACC_Y] * ACC_Y_REV),
        z: (result[ACC_Z] * ACC_Z_REV)
      }
      //console.log ("acc  x:"+acc.x+" y:"+acc.y+"  z:"+acc.z);
      // Now read the compass
      xloborg.readCompass(function(err, result){
        if (err){
          console.error(err);
        } else {
          var comp = {
            x: result[0], y: result[1], z: result[2]
          };
          var heading = 180 * Math.atan2(comp.y,comp.x) / PI;
          if(heading < 0) heading += 360;
          sensorReadings.comp = comp;
          sensorReadings.comp.heading = heading;
          
          //console.log ("heading:"+heading+"  x:"+compass.x+" y:"+compass.y+"  z:"+compass.z);
          sensorTransmit(sensorReadings);
        }
      });
    }
  });
}

function sensorTransmit(readings) {
    io.emit('data', readings);
}


