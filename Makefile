
.PHONY: all install uninstall
all:
	npm install
  
install: 
	[ "`id -u`" = "0" ] || { echo "Must be run as root"; exit 1; }
	##cp -f server.js /usr/local/sbin
	cp -f init-script /etc/init.d/bals-rov
	chmod 755 /etc/init.d/bals-rov
	update-rc.d bals-rov defaults 92 08
	/etc/init.d/bals-rov start

uninstall:
	[ "`id -u`" = "0" ] || { echo "Must be run as root"; exit 1; }
	[ -e /etc/init.d/bals-rov ] && /etc/init.d/bals-rov stop || :
	update-rc.d bals-rov remove
	##rm -f /usr/local/sbin/server.js
	rm -f /etc/init.d/bals-rov

clean:
	##rm -f server.js

